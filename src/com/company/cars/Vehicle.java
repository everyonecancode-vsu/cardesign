package com.company.cars;

import com.company.breaking.Breaking;
import com.company.cargo.Cargo;
import com.company.components.Component;
import com.company.components.Detail;
import com.company.components.DetailAssembly;
import com.company.components.Engine;
import com.company.components.Material;
import com.company.driving.Accelerartion;
import com.company.driving.Gear;
import com.company.driving.SteeringAngle;
import com.company.finance.Money;
import com.company.geo.Drive;
import com.company.service.VehicleService;

/**
 * Abstract class for all cars
 */
public abstract class Vehicle {

    /**
     * Component of the car (private field)
         *
         * @see Component
         */
        private Component[] mComponent;

    



    /**
     * Realization of the driving opportunities
     *
     * @param acceleration
     * @param gear
     * @param steeringAngle
     * @return
     */
    public abstract Drive drive(
            Accelerartion acceleration,
            Gear gear,
            SteeringAngle steeringAngle
    );

    /**
     * Realization of the car's repairing on the service
     *
     * @param newBreaking
     * @return
     */
    public abstract Money repair(Breaking newBreaking);

    /**
     * Realization of the car's ownership
     *
     * @param service
     * @return
     */
    public abstract Money manage(VehicleService service);

    /**
     * Realization of the delivering people and their luggage or other cargo by car
     *
     * @param cargo
     * @return
     */
    public abstract Money deliver(Cargo cargo);

}
