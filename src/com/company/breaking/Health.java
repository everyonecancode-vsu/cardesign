package com.company.breaking;
import com.company.components.Component;
import com.company.components.Connection;

import java.util.Random;

public class Health {
    /*
    * Attributes of Health
     **/
    public double probabilityOfBreaking;

    /*
    * Constructor
     **/
    Health() {
        this(0);
    }

    Health(double probabilityOfBreaking) {
        this.probabilityOfBreaking = probabilityOfBreaking;
    }
    /*
    * Set probability of breaking:
     **/
    public void setProbabilityOfBreaking(double probabilityOfBreaking){
        this.probabilityOfBreaking = probabilityOfBreaking;
    }
    /*
    * Get probability of breaking:
     **/
    public double getProbabilityOfBreaking() {
        return probabilityOfBreaking;
    }

    /*
    * Set default probability of breaking = 0.01:
     */
    public void setDefaultProbabilityOfBreaking(){
        this.probabilityOfBreaking = 0.01;
    }

    /*
    This method increases probability of breaking of detail.
    It should be used by receiving of message of usage of detail.
     */
    public double increaseProbabilityOfBreaking() {
        if (probabilityOfBreaking < 0.4){
            probabilityOfBreaking += 0.01;
        }
        if (probabilityOfBreaking >= 0.4) {
            if (probabilityOfBreaking < 0.6) {
                probabilityOfBreaking += 0.02;
            }
        }
        if (probabilityOfBreaking >= 0.6){
            if (probabilityOfBreaking < 0.85){
                probabilityOfBreaking += 0.03;
                }
        }
        if (probabilityOfBreaking >= 0.85){
            probabilityOfBreaking += 0.05;
        }
    return probabilityOfBreaking;
    }

    /*
    This method breaks detail depending on the (probabilityOfBreaking)
     */
    public boolean killDetailOrNot() {
        boolean result = false;
        Random rnd = new Random();
        int randomGeneratedNumber = rnd.nextInt(100);
        double randomGeneratedNumberForCompare = randomGeneratedNumber / 100.0;
        /*probabilityOfBreaking [0...1] is double
        that's why we create new int probabilityOfBreakingForArray = probabilityOfBreaking*100 and round it
         */
        // Convert math-probability to "life-probability":
        int probabilityOfBreakingForArray = (int) Math.round(probabilityOfBreaking * 10);
        double[] array = new double[probabilityOfBreakingForArray];
        for (int i = 0; i < array.length; i++) {
            double toArrayDouble = (double) (i + 1) / 100.0;
            array[i] = toArrayDouble;
        }
        int counter;
        for (counter = 0; counter < array.length; counter++) {
            if (array[counter] == randomGeneratedNumberForCompare) {
                //then detail becomes broken
                result = true;
            }
        }
        return result;

    }
}

