package com.company.breaking;

import com.company.components.Detail;

import java.util.ArrayList;
import java.util.Collection;

public class DetailHealth extends Health {

    ArrayList<DetailHealth> arrayOfDetailHealth = new ArrayList<>();
    Collection arrayOfDetails = new ArrayList(); // Details objects must be imported from com.company.components

    public ArrayList<DetailHealth> setDefaultProbabilityToAllConnections() {
        arrayOfDetailHealth.addAll(arrayOfDetails);
        for (int i = 0; i < arrayOfDetailHealth.size(); i++) {
            arrayOfDetailHealth.get(i).setDefaultProbabilityOfBreaking();
        }
        return arrayOfDetailHealth;
    }

    public ArrayList<DetailHealth> createBreakings() {
        ArrayList<DetailHealth> listOfDetailBreakings = new ArrayList<>();
        for (int i = 0; i < arrayOfDetailHealth.size(); i++) {
            DetailHealth exemplar = arrayOfDetailHealth.get(i);
            if (exemplar.killDetailOrNot() == true) {
                listOfDetailBreakings.add(exemplar);
            }
            if (exemplar.getProbabilityOfBreaking() == 1){
                listOfDetailBreakings.add(exemplar);
            }
        }
        return listOfDetailBreakings;
    }

}