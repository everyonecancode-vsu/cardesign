package com.company.breaking;

import com.company.components.Connection;

import java.util.ArrayList;
import java.util.Collection;

public class ConnectionHealth extends Health {

    ArrayList<ConnectionHealth> arrayOfConnectionHealth = new ArrayList<>();
    Collection arrayOfConnections = new ArrayList(); // Connection objects must be imported from com.company.components

    public ArrayList<ConnectionHealth> setDefaultProbabilityToAllConnections() {
        arrayOfConnectionHealth.addAll(arrayOfConnections);
        for (int i = 0; i < arrayOfConnectionHealth.size(); i++) {
            arrayOfConnectionHealth.get(i).setDefaultProbabilityOfBreaking();
        }
        return arrayOfConnectionHealth;
    }

    public ArrayList<ConnectionHealth> createBreakings() {
        ArrayList<ConnectionHealth> listOfConnectionBreakings = new ArrayList<>();
        for (int i = 0; i < arrayOfConnectionHealth.size(); i++) {
            ConnectionHealth exemplar = arrayOfConnectionHealth.get(i);
            if (exemplar.killDetailOrNot() == true) {
                listOfConnectionBreakings.add(exemplar);
            }
            if (exemplar.getProbabilityOfBreaking() == 1){
                listOfConnectionBreakings.add(exemplar);
            }
        }
        return listOfConnectionBreakings;
    }

}