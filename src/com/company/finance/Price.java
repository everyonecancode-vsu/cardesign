package com.company.finance;
/*
A class containing prices for parts, services, taxes, and etc.
 */
public class Price {
    /*
    Money used for payment.
     */
    private Money vlt;
    private double value;
    public Price(double val, Money V) {
        this.value=val;
        this.vlt=V;
    }
    public double getPriceValue(char currency){
        return String("Цена равна", this.value * this.vlt);
    }
}