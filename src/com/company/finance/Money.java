package com.company.finance;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * Money's class (for calculating ownership and repairing costs)
 */
public class Money {
    private String vlt;
    public double course;

    Money (String V) {
        this.vlt=V;
        this.course=this.getCourse(V);
    }
    public double toAnother(String V) {
        return this.getCourse(V)/this.course;
    }
    public static String getContentOfHTTPPage(String pageAddress, String codePage) throws Exception {
        StringBuilder sb = new StringBuilder();
        URL pageURL = new URL(pageAddress);
        URLConnection uc = pageURL.openConnection();
        BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        uc.getInputStream(), codePage));
        try {
            String inputLine;
            while ((inputLine = br.readLine()) != null) {
                sb.append(inputLine);
            }
        }
        finally {
            br.close();
        }
        return sb.toString();
    }
    public double getCourse(String vlt){
        try {
            ListOfCourse loc = new ListOfCourse();
            Gson gson = new Gson();
            String json = Money.getContentOfHTTPPage("https://www.cbr-xml-daily.ru/daily_json.js","UTF8");
            loc = (ListOfCourse) gson.fromJson(json, loc.getClass());
            this.course = Double.parseDouble(String.valueOf(loc.Valute.get(vlt).Value));
            return this.course;
        }
        catch (Exception ex) {
            Logger.getLogger(Money.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }
}