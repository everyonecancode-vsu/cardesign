package com.company.geo;

/**
 * Basic class for determination of the car's direction
 */
public class Direction {

    private double mAzimut;

    /**
     * Getter of the car's direction
     */
    public double getDirection() {
        return this.mAzimut;
    }

    /**
     * Setter of the new direction
     *
     * @param newAzimut - new direction in angle according to North
     */
    public void setDirection(double newAzimut) {
        this.mAzimut = newAzimut;
    }

    /**
     * Overrided magic method of the Object (output to the String format)
     *
     * @return
     */
    @Override
    public String toString() {
        return "Azimut of the vehicle: " + String.valueOf(this.mAzimut);
    }
}
