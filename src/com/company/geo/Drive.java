package com.company.geo;

/**
 * Aggregated class for determination of the general car's location
 */
public class Drive {
    public Direction mDirection;
    public Location mLocation;
    public Speed mSpeed;
}
