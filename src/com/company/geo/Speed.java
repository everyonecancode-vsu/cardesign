package com.company.geo;

/**
 * Basic class for determination of the car's speed
 */
public class Speed {

    /**
     * Current car's speed (private field)
     */
    private double mSpeed;

    /**
     * Getter of the current car's speed
     *
     * @return
     */
    public double getSpeed() {
        return this.mSpeed;
    }

    /**
     * Setter of the current car's speed
     */
    public void setSpeed(double newSpeed) {
        this.mSpeed = newSpeed;
    }

    /**
     * Overrided magic method of the Object (output to the String format)
     *
     * @return
     */
    @Override
    public String toString() {
        return "Speed of the vehicle: " + String.valueOf(this.mSpeed);
    }
}
