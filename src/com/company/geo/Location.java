package com.company.geo;

/**
 * Basic class for determination of the car's location
 */
public class Location {

    /**
     * Latitude of the current car's location
     */
    private double mLatitude;

    /**
     * Longitude of the current car's location
     */
    private double mLongitude;

    /**
     * Getter of the current latitude
     */
    public double getLatitude() {
        return this.mLatitude;
    }

    /**
     * Getter of the current longitude
     */
    public double getLongitude() {
        return this.mLongitude;
    }

    /**
     * Setter of the new location
     *
     * @param newLatitude
     * @param newLongitude
     */
    public void setLocation(double newLatitude, double newLongitude) {
        this.mLatitude = newLatitude;
        this.mLongitude = newLongitude;
    }

    /**
     * Overrided magic method of the Object (output to the String format)
     *
     * @return
     */
    @Override
    public String toString() {
        return "Location of the vehicle\nLat: " + String.valueOf(this.mLatitude)
                                    + "\nLon: " + String.valueOf(this.mLongitude);
    }
}
