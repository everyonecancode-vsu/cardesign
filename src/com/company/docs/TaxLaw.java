package com.company.docs;
import com.company.Math.Parser;
import com.company.finance.Money;
import com.company.finance.Price;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

class TaxLaw{
    private Money currency;
    private String formula;
    private URL lawTextURL;
    public TaxLaw (String formula_ , URL lawTextURL_, Money currency_){
        this.formula=formula_;
        this.currency=currency_;
        this.lawTextURL=lawTextURL_;
    }
    public Price callTax(double[] pars) {
        double val = this.parse();
        return new Price(val, this.currency);
    }

    public double parse() {
        double pars = 0.0;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Parser myParser = new Parser();
        try {
            String formula_ = reader.readLine();
            pars = myParser.evaluate(formula_);
        }
        catch (Exception e) {
            System.out.println(e);
        }
        return pars;
    }
}