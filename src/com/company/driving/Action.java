package com.company.driving;

import com.company.breaking.Breaking;
import com.company.breaking.Wear;

/**
 * Abstract class of all type of the user's actions in the car
 */
public abstract class Action {

    /**
     * Wear by action
     */
    private Wear wear;

    /**
     * Wrong action effect
     */
    private Breaking breaking;

    /**
     * Adding some wear by new action
     *
     * @return
     */
    public abstract Wear addWear();

    /**
     * Producing a breaking
     *
     * @return
     */
    public abstract Breaking produceBreaking();

}
