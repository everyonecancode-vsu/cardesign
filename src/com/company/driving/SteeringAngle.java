package com.company.driving;

import com.company.breaking.Breaking;
import com.company.breaking.Wear;

/**
 * Steering angle of the car
 */
public class SteeringAngle extends Action {

    @Override
    public Wear addWear() {
        return null;
    }

    @Override
    public Breaking produceBreaking() {
        return null;
    }
}
