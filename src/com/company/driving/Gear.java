package com.company.driving;

import com.company.breaking.Breaking;
import com.company.breaking.Wear;

/**
 * Current gear of the transmission
 */
public class Gear extends Action {

    /**
     * Current gear of the car (private field)
     */
    private byte mGear;

    /**
     * Maximum gear
     */
    private byte maxGear;

    /**
     * Default constructor
     *
     * @param maxGear
     */
    Gear(byte maxGear) {
        this.maxGear = maxGear;
        this.mGear = 0;
    }

    /**
     * Select neutral gear of the transmission
     *
     * @return
     */
    public byte neutralTransmission() {
        this.mGear = 0;
        return this.mGear;
    }

    /**
     * Increasing a gear of the transmission
     *
     * @return
     */
    public byte increaseTransmission() {
        this.mGear += (this.mGear < this.maxGear ? 1 : 0);
        return this.mGear;
    }

    /**
     * Decreasing a gear of the transmission
     *
     * @return
     */
    public byte decreaseTransmission() {
        if (this.mGear > 0) this.mGear -= 1;
        return this.mGear;
    }

    /**
     * Select back gear of the transmission
     *
     * @return
     */
    public byte backTransmission() {
        this.mGear = -1;
        return this.mGear;
    }

    @Override
    public Wear addWear() {
        return null;
    }

    @Override
    public Breaking produceBreaking() {
        return null;
    }
}
